#! /bin/bash
# ASIX M01
# Curs 2022-2023
# 23/01/2023
#
# exemple 7: validar que sigui un directori
# ----------------------------------------------------------
ERR_ARGS=1
ERR_NODIR=2

# 1) validar el número d'arguments
if [ $# -ne 1 ]
then
  echo "Error número d'arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_ARGS
fi

# 2) validar directori
if ! [ -d $1 ]
then
  echo "Error: $1 no és un directori"
  echo "Usage: $0 dir"
  exit $ERR_NODIR
fi

# 3) xixa
directori=$1
ls $directori

exit 0
