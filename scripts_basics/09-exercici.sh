#! /bin/bash
# ASIX M01
# Curs 2022-2023
# Saray Cordon Arnau
#
# Programa que rep per stdin noms d'usuaris, si existeixen en el sistema 
# mostra el nom de stdout. Si no existeix el mostra per stderr
# -------------------------------------------------------------------------
while read -r users
do
  grep -q "^$users:" /etc/passwd
  if [ $? -eq 0 ]
  then
    echo $users
  else
    echo $users >> /dev/stderr
  fi
done
exit 0

