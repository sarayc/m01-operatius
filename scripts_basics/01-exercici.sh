#! /bin/bash
# @edt ASIX 
# Curs 2022-2023
# Saray Cordon Arnau
# 
# Mostrar l'entrada estàndard numerant línia a línia
# ---------------------------------------------------------
while read -r line
do
  echo $line
done
exit 0
