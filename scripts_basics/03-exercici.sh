#! /bin/bash
# @edt ASIX 
# Curs 2022-2023
# Saray Cordon Arnau
# 
# Fes un comptador des de zero fins al valor indicat per l'argument rebut
# --------------------------------------------------------------------------
num=0
MAX=$1
while [ $num -le $MAX ]
do
  echo $num
  ((num++))
done
exit 0
