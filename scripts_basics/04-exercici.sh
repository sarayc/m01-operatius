#! /bin/bash
# @edt ASIX 
# Curs 2022-2023
# Saray Cordon Arnau
# 
# Fes un programa que rep com a arguments números de mes (un o més)
# i indica per a cada mes rebut quantes dies té el mes.
# prog mes1 ...
# -------------------------------------------------------------------
ERR_NARGS=1
ERR_NOMES=2
# 1) validar el número d'arguments
if [ $# -eq 0 ]
then
  echo "Error: número d'arguments incorrecte"
  echo "Usage: $0 mes"
  exit $ERR_NARGS
fi

# 2) validar mes i xixa
for mes in $*
do
  if ! [ $mes -ge 1 -a $mes -le 12 ]
  then
    echo "Error: mes $mes no vàlid [1-12]" >> /dev/stderr
  else
    case $mes in 
      "4"|"6"|"9"|"11")
        dies=30;;
      "2")
         dies=28;;
      *)
        dies=31;;
    esac
    echo "El mes $mes té $dies"
  fi
done
exit 0
