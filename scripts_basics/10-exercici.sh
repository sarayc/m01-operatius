#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau

# Programa que rep com a argument un número indicatiu del número màxim de
# línies a mostrar. El programa processa stdin línia a línia i mostra numerades
# un màxim de num línies 
# ------------------------------------------------------------------------

num=1
MAX=$1
while read -r line
do	
  if [ "$num" -le $MAX ]; then
    echo "$num: $line"
  else
    echo "$line"      	  
  fi	  
  num=$((num+1))
done

