#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau

# Processar línia a línia l'entrada estàndard, si la línia té més
# de 60 caràcters la mostra, si no no.
# -------------------------------------------------------------------
while read -r line
do
  caracters=$(echo "$line" | grep -E "^.{60,}")
  if [ $? -eq 0 ] 
  then
    echo $line
  fi
done
exit 0

# ---------------------------------------
while read -r line
do
  caracters=$(echo "$line" | wc -c)
  if [ "$caracters" -ge 60 ] 
  then
    echo $line
  fi
done
exit 0

