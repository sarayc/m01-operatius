#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau

# Programa que rep com a argument noms d'usuaris, si existeixen en el 
# sistema mostra el nom per stdout. Si no existeix el mostra per stderr.
# ------------------------------------------------------------------------
for users in $*
do
  grep -q "^$users:" /etc/passwd 
  if [ $? -eq 0 ]
  then
    echo $users
  else
    echo $users >> /dev/stderr
  fi
done
exit 0

