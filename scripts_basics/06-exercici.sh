#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau
# 
# Programa que rep com a arguments noms de dies de la setmana
# i mostra quants dies eren laborables i quants festius.
#  $ prog dia_setmana...
# ----------------------------------------------------------
laborables=0
festius=0
for dies_mes in $*
do
  case $dies_mes in 
    "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
      ((laborables++));;
    "dissabte"|"diumenge")
      ((festius++));;
    *)
      echo "Error: $dies_mes no és un dia de la setmana" >> /dev/stderr
  esac
done
echo "$laborables dies setmana laborables"
echo "$festius dies setmana festius"
exit 0
