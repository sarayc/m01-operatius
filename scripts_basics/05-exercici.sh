#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau

# Mostra línia a línia l'entrada estàndard, retallant només 
# els primers 50 caràcters
#  $ prog 
# ----------------------------------------------------------
while read -r line
do
  echo $line | cut -c1-50 
done
exit 0
