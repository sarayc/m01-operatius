#! /bin/bash
# @edt ASIX 
# Curs 2022-2023
# Saray Cordon Arnau
# 
# Mostrar els arguments rebuts línia a línia, tot numerànt-los
# ---------------------------------------------------------------
num=1
for arguments in $*
do
  echo "$num: $arguments"
  ((num++))
done
exit 0
