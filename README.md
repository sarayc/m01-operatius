# Scripts 2022-2023

## @edt ASIX-m01

Llistat d'exercicis de M01 operatius scripts

Per baixar/clonar el repositori
```
git clone https://gitlab.com/sarayc/m01-operatius.git
```

Per descarregar
```
git pull
```

Per pujar els apunts
```
git add .; git commit -m "exercicis" ; git push 
```
