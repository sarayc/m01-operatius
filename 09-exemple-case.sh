#! /bin/bash
# ASIX M01
# Curs 2022-2023
# 23/01/2023
#
# Exemple case
# ----------------------------------------------------------

# dl dt dc dj dv --> laborable
# ds dm --> festiu
# altre

case $1 in 
  d[ltcjv])
    echo "$1 és un dia laborable";;
  d[sm])
    echo "$1 és un dia festiu";;

  *)
    echo "$1 és un altra cosa"
esac
exit 0

# ---------------------------------------------------------
case $1 in
         [aeiou])
		echo "$1 és un vocal";;
	[bcdfghjklmnpqrstvwxyz])
		echo "$1 és una consonant";;
	*)
		echo "$1 és una altra cosa";;
esac
exit 0

# --------------------------------------------------------

case $1 in 
  "pere"|"pau"|"joan") 
     echo "és un nen" ;;

  "marta"|"anna"|"julia")
     echo "és una nena";;
   *)
     echo "no binari"
esac
exit 0

# -------------------------------------------------------
