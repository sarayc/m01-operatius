#! /bin/bash
# ASIX M01
# Curs 2022-2023
# 23/01/2023
#
# Descripcio: exemples bucle for
# ----------------------------------------------------------

# 8) llistar tots els logins numerats
llista_logins=$(cut -d: -f1 /etc/passwd | sort)
num=1

for login in $llista_logins
do
  echo "$num: $login"
  ((num++))
done
exit 0

# ----------------------------------------------------------

# 7) llista els fitxers del directori actiu numerats
llistat=$(ls)
num=1
for nom in $llistat
do
  echo "$num: $nom"
  num=$((num+1))
  # ((num++))
done
exit 0

# --------------------------------------------------------

# 6) numerar els arguments
num=1
for arguments in $*
do
  echo "$num: $arguments"
  num=$((num+1))
done
exit 0

# --------------------------------------------------------

# 5) iterar i mostrar la llista d'arguments (una iteració)
for arguments in "$@"
do
  echo $arguments
done
exit 0

# ------------------------------------------------------

# 4) iterar i mostrar la llista d'arguments
for arguments in $*
do
  echo $arguments
done
exit 0

# -------------------------------------------------------

# 3) iterar pel valor d'una variable
llistat=$(ls)
for nom in $llistat
do
  echo "$nom"
done
exit 0

# ---------------------------------------------------------

# 2) iterar per un conjunt d'elements (una iteració)
for nom in "pere marta pau anna"
do
  echo "$nom"
done
exit 0

# ------------------------------------------------------

# 1) iterar per un conjunt d'elements
for nom in "pere" "marta" "pau" "anna"
do
  echo "$nom"
done
exit 0
