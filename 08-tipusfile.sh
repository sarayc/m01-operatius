#! /bin/bash
# ASIX M01
# Curs 2022-2023
# 23/01/2023
#
# exemple 7: validar que sigui un directori
# ----------------------------------------------------------
ERR_ARGS=1
ERR_NODIR=2

# 1) validar el número d'arguments
if [ $# -ne 1 ]
then
  echo "Error número d'arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_ARGS
fi

# 2) xixa
argument=$1
if [ ! -e $argument ]
then
  echo "L'argument $argument no existeix"

elif [ -h $argument ]
then
  echo "L'argument $argument és un regular file"

elif [ -d $argument ]
then
  echo "L'argument $argument és un directori"

elif [ -f $argument ]
then
  echo "L'argument $argument és un link"

else
  echo "L'argument $argument és un altre cosa"
  
fi
exit 0         
