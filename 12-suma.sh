#! /bin/bash
# @edt ASIX-M01
# Març 2023
# 
# Programa que rep un o més arguments, ens calcula i mostra la 
# suma de tots aquests valors
#  $ prog num1 ... 	# synopsis
# ----------------------------------------------------------
ERR_ARGS=1

# 1) validar el número d'arguments
if [ $# -eq 0 ]
then
  echo "Error: número d'arguments no vàlid"
  echo "Usage: $0 num1 ..."
  exit $ERR_ARGS
fi

# 2) xixa
suma=0

for numeros in $*
do
  suma=$((suma+numeros))
#  echo $suma
done
echo $suma
exit 0
