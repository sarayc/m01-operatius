#! /bin/bash
# @edt ASIX-M01
# Març 2023
# 
# Programa que rep una o més notes d'un alumne i indica per cada nota
# rebuda si és suspès, aprovat, notable o excel·lent. 
#  $ prog notes...
# ----------------------------------------------------------
ERR_ARGS=1

if [ $# -eq 0 ]
then
  echo "Error: número d'arguments"
  echo "Usage: $0 nota"
  exit $ERR_ARGS
fi

# 2) xixa
for nota in $*
do
  if ! [ $nota -ge 0 -a $nota -le 10 ]
  then
    echo "Error: nota $nota no vàlida [0-10]" >> /dev/stderr

  elif [ $nota -lt 5 ]
  then
    echo "La nota $nota és suspès"

  elif [ $nota -lt 7 ]
  then
    echo "La nota $nota és aprovat"

  elif [ $nota -lt 9 ]
  then
    echo "La nota $nota és notable"

  else
    echo "La nota $nota és excel·lent"
  fi
done
exit 0

