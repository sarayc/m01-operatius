#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
# 
# Exemple validar arguments: programa que valida que siguin dos arguments
#  $ prog nom edat 
# ----------------------------------------------------------
# 1) validar que existeix dos argument
if [ $# -ne 2 ]
then
  echo "Error: número d'arguments incorrecte"
  echo "Usage: $0 nom edat"
  exit 1
fi

# 2) xixa

echo "nom: $1"
echo "edat: $2"
exit 0
