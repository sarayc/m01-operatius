#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordón Arnau
#
# 17-argsfor.sh [-a -b -c -d] arg...
# Aculumar les opcions i els arguments
# ----------------------------------------------------------------------
opcions=""
arguments=""
for arg in $*
do
  case $arg in
    "-a"|"-b"|"-c"|"-d")
      opcions="$opcions $arg";;
    *)
      arguments="$arguments $arg";;
  esac
done
echo "Opcions: $opcions"
echo "Arguments: $arguments"
exit 0

# ------------------------------------

for arg in $*
do
  case $arg in
    "-a"|"-b"|"-c"|"-d")
      echo "opcio:$arg";;
    *)
      echo "arguments: $arg";;
  esac
done
exit 0
