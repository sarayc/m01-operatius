#! /bin/bash
# ASIX M01
# Curs 2022-2023
# 23/01/2023
#
# Validar que rep un argument i que és un directori i llistar-ne el contingut. 
# Ens digui si és un link, regular file, directori o altra cosa.

# sinopsis: prog dir
# ----------------------------------------------------------------------------
ERR_NARGS=1
ERR_NODIR=2
# 1) validar número d'arguments
if [ $# -eq 0 ]
then
  echo "Error: número arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# 2) xixa
for directori in $*
do
  if ! [ -d $directori ]
  then
    echo "Error: no és un directori" 2> /dev/stderr
  else
    echo "$directori"
    llista=$(ls $directori)
    for element in $llista
    do
      if [ -L "$directori/$element" ] 
      then
        echo -e "\t $element és un Link"
      elif [ -f "$directori/$element" ]
      then
        echo -e "\t $element és un regular file"
      elif [ -d "$directori/$element" ]
      then
        echo -e "\t$element és un directori"
      else
        echo -e "\t$element és un altra cosa"
      fi 
    done
  fi
done
exit 0
