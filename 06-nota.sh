#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
# 
# Exemple validar nota: programa que valida si una nota està aprovada o suspès
#  $ prog nota
# ----------------------------------------------------------
ERR_NARGS=1
ERR_NOTA=2

# 1) validar número d'arguments
if [ $# -ne 1 ]
then
  echo "Error: número d'arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS 
fi

# 2) validar nota no és vàlida
if ! [ $1 -ge 0 -a $1 -le 10 ]
then
  echo "Error: nota $1 no valida"
  echo "nota pren valors de 0 a 10"
  echo "Usage: $0 nota"
  exit $ERR_NOTA
fi

# 3) xixa
nota=$1
if [ $nota -lt 5 ]
then
  echo "Nota $nota: suspès"

elif [ $nota -lt 7 ]
then
  echo "Nota $nota: aprovat"

elif [ $nota -lt 9 ]
then
  echo "Nota $nota: notable"

else
  echo "Nota $nota: excel·lent"
 
fi
exit 0
