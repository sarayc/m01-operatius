#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau

# Rep per stdin GIDs i llista per stdout la informació de cada un d'aquests
# grups, en format: gname:GNAME,gid:GID,users:USERS
# ------------------------------------------------------------------------
for gid in $* 
do
  # filtrar pel gid del /etc/group 
  llista_gid=$(grep "^[^:]*:[^:]*:$gid:" /etc/group)

  # retallem els camps
  gname=$(echo $llista_gid | cut -d: -f1 | tr 'a-z' 'A-Z')
  users=$(echo $llista_gid | cut -d: -f4 | tr 'a-z' 'A-Z') 
  echo "gname: $gname, gid: $gid, users: $users"
done
exit 0

