#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau

# Rep per stdin GIDs i llista per stdout la informació de cada un d'aquests
# grups, en format: gname:GNAME,gid:GID,users:USERS
# ------------------------------------------------------------------------

while read -r gid 
do
  # filtrar pel gid del /etc/group 
  llista_gid=$(grep "^[^:]*:[^:]*:$gid:" /etc/group)

  # retallem els camps
  gname=$(echo $llista_gid | cut -d: -f1 | tr 'a-z' 'A-Z')
  users=$(echo $llista_gid | cut -d: -f4 | tr 'a-z' 'A-Z') 
  echo "gname: $gname, gid: $gid, users: $users"
done
exit 0

# --------------------------------------------------------------------------

status=0
while read -r line
do
  info=$(grep -E "^[^:]*:[^:]:$line:" /etc/group)
  if ! [ $? -eq 0 ]
  then
    status=2
    echo "Error: $line no és un gid del fitxer /etc/group" >> /dev/stderr
  else
    format=$(grep -E "^[^:]*:[^:]:$line:" /etc/group | 
	     cut -d: -f1,3,4| tr 'a-z' 'A-Z' | 
	     sed -r 's/^(.*):(.*):(.*)/gname: \1, gid: \2, users: \3/') 
    echo $format
  fi
done
exit 0
