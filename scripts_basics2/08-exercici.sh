#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau

# Programa que per cada file el comprimeix. Generar per stdout el nom del file
# comprimit si s'ha comprimit, sinó genera un missatge d'error per stderr. 
# Mostra també per stdout quantes files s'han comprimit. 
# -------------------------------------------------------------------------
ERR_NOARGS=1
# 1) Validar el número arguments
if [ $# -eq 0 ]
then
  echo "Error: número arguments incorrecte"
  echo "Usage: $0 file..."
  exit $ERR_NOARGS
fi

# 2) xixa
status=0
num=0
for fitxer in $*
do
  comprimir=$(gzip $fitxer &> /dev/null)
  if [ $? -eq 0 ]
  then
    echo $fitxer
    ((num++))
  else
    echo "Error: no s'ha pogut comprimir" >> /dev/stderr
    status=2
  fi
done
echo "Número de fitxers comprimits: $num"
exit $status

