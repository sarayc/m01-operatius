#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau

# Processar stdin mostrant per stdout les línies de menys de 50 caràcters
# -------------------------------------------------------------------------
while read -r line
do
  caracters=$(echo $line | grep -E "^.{50,}")
  if [ $? -ne 0 ]
  then
    echo $line
  fi
done
exit 0
