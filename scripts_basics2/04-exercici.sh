#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau

# Processar stdin mostrant per stdout les línies numerades i en majúscules
# -------------------------------------------------------------------------
num=1
while read -r line
do
  echo "$num: $line" | tr 'a-z' 'A-Z'
  ((num++))
done
exit 0
