#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau

# programa -h uid...
# Per a cada uid mostra la informació de l'usuari en format:
# logid(uid) gname home shell
# ------------------------------------------------------------------------
for uid in $*
do
  llista_uid=$(grep "^[^:]*:[^:]*:$uid:" /etc/passwd)
  login=$(echo $llista_uid | cut -d: -f1)
  home=$(echo $llista_uid | cut -d: -f6)
  shell=$(echo $llista_uid | cut -d: -f7)
  
  gid=$(echo $llista_uid | cut -d: -f4)
  llista_gid=$(grep "^[^:]*:[^:]*:$gid:" /etc/group)
  gname=$(echo $llista_gid | cut -d: -f1)
  echo "$login($uid) $gname $home $shell"
done
exit 0

