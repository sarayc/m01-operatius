#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau

# 
# -------------------------------------------------------------------------

opcions=""
cognom=""
edat=""
arguments=""

while [ -n "$1" ]
do
  case $1 in 
    -c)
      cognom="$2 $3"
      shift
      shift;;

    -f)
      edat=$2
      shift;;

    -r|-m|-j)
      opcions="$opcions$1";;

    *)
      arguments="$arguments$1";; 
  esac
  shift
done
echo "Les opcions són: $opcions"
echo "El cognom és: $cognom"
echo "L'edat és: $edat"
echo "Els arguments són: $arguments"
exit 0
