#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau

# Processar els arguments que són matrícules
# a) llistar les vàlides del tipus: 9999-AAA
# -------------------------------------------------------------------------

for arguments in $*
do
  es_valid=$(echo $arguments | grep -E "^[0-9]{4}-[A-Z]{3}$")
  if [ $? -eq 0 ]
  then
    echo $es_valid
  fi
done
exit 0
