#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau

# Programa que valida que els quatre arguments rebuts són tots del tipus
# que indica el flag. 
# -------------------------------------------------------------------------
ERR_ARGS=1
status=0
if [ $# -ne 5 ]
then
  echo "Error: número d'arguments no vàlid"
  echo "Usage: $0 args"
  exit $ERR_ARGS
fi

if ! [ $1 == "-f" -o $1 == "-d" ]
then
  echo "Error: no és un directori ni un fitxer"
  echo "Usage: $0 args"
  exit $ERR_ARGS
fi

args=$(echo $* | cut -d ' ' -f2-)

if [ $1 == "-f" ]
then
  for arguments in $args
  do
    if ! [ -f $arguments ]
    then
      status=2
    fi
  done

elif [ $1 == "-d" ]
then
  for arguments in $args
  do
    if ! [ -d $arguments ]
    then
      status=2
    fi
  done
else
  status=2
fi

exit $status
