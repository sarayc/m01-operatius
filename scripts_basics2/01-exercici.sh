#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau

# Processar els arguments i mostrar per stdout només els de 4 o més 
# caràcters
#  $ prog args...
# ----------------------------------------------------------
for arguments in $*
do
  caracters=$(echo $arguments | wc -c)
  if [ $caracters -gt 4 ]
  then 
    echo $arguments
  fi
done
exit 0
