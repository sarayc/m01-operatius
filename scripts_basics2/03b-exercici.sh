#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau

# Processar els arguments que són matrícules
# b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status
# el número d'errors (de no vàlides)
# -------------------------------------------------------------------------
num_errors=0
for arguments in $*
do
  es_valid=$(echo $arguments | grep -E "^[0-9]{4}-[A-Z]{3}$")
  if [ $? -eq 0 ]
  then
    echo $arguments
  else
    echo "Error: $arguments matrícula no vàlida">> /dev/stderr
    ((num_errors++))
    
  fi
done
echo "Hi ha $num_errors número de matrícules incorrectes"
exit $num

