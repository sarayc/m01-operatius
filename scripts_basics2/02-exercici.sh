#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau

# Processar els arguments i comptar quantes n'hi ha de 3 o més caràcters.
# -------------------------------------------------------------------------
num=0
for arguments in $*
do
  caracters=$(echo $arguments | grep -E "^.{3,}")
  if [ $? -eq 0 ]
  then
    ((num++))
  fi
done
echo "Hi ha $num de tres o més caràcters"
exit 0

