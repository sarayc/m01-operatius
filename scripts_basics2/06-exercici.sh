#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordon Arnau

# Processar stdin mostrant per stdout les línies de menys de 50 caràcters
# -------------------------------------------------------------------------
while read -r line
do
  inicial=$(echo $line | cut -c1)
  cognom=$(echo $line | cut -d' ' -f2)
  echo "$inicial.$cognom"
done
exit 0

# -----------------------------------------------------------------
while read -r line
do
  inicial=$(echo $line | cut -c1)
  cognom=$(echo $line | sed -r 's/^(.*) (.*)$/\2/' )
  echo "$inicial.$cognom"
done
exit 0
