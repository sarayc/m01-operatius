#! /bin/bash
# ASIX M01
# Curs 2022-2023
# 23/01/2023
#
# Validar que rep un argument i que és un directori i llistar-ne el contingut
# per llistar-ne el contingut amb un simple ls ja n'hi ha prou.

# sinopsis: prog dir
# ----------------------------------------------------------------------------
ERR_NARGS=1
ERR_NODIR=2
# 1) validar número d'arguments
if [ $# -ne 1 ]
then
  echo "Error: número arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# 2) validar que és un directori
directori=$1
if ! [ -d $directori ]
then
  echo "Error: no és un directori"
  echo "Usage: $0 dir"
  exit $ERR_NODIR
fi

# 3) xixa
llista=$(ls $directori)
num=1
for element in $llista
do
  echo "$num: $element"
  ((num++))
done
exit 0
