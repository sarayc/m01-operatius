#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordón Arnau
#
# Programa que afegeix a cada usuari al grup indicat. Si es produeix error
# mostrar per stderr. Retorna el número d'errors.
# prog grup users...
# ----------------------------------------------------------------------
error=0
grup=$1
shift
for login in $*
do
  usermod -G $grup $login &> /dev/null
  if [ $? -ne 0 ]
  then
    echo $login >> /dev/stderr
    ((error++))
  fi
done
exit $error
