#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordón Arnau
#
# Ierar per llista d'arguments i fer una acció, mostra ok si l'acció 
# ha anat bé i malament si ha fallat
# prog args...
# ----------------------------------------------------------------------
for uid in $*
do
  grep -q "^[^:]*:[^:]*:$uid:" /etc/passwd
  if [ $? -eq 0 ]
  then
    echo "ok"
  else
    echo "malament"
  fi
done
exit 0
