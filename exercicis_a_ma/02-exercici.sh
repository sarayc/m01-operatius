#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordón Arnau
#
# Ierar la llista de logins, si existeix stdout si no existeix stderr mostra
# per pantalla el total, els oks, i els errors. Retorna 0 si tot ha anat bé, 
# 1 si algún error. 
# prog logins...
# ---------------------------------------------------------------------------
status=0
ok=0
error=0
for login in $*
do
  grep -q "^$login:" /etc/passwd
  if [ $? -eq 0 ]
  then
    echo $login
    ((ok++))
  else
    echo $login >> /dev/stderr
    ((error++))
    status=1
  fi
done
echo -e " Totals: $# \n Oks: $ok \n Errors: $error"
exit $status
