#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
# 
# Exemple if: indicar si és major d'edat
#  $ prog edat (sinopsis)
# ----------------------------------------------------------
# 1) validar que existeix un argument
if [ $# -ne 1 ]
then
  echo "Error: numero args incorrecte"
  echo "Usage: $0 edat"
  exit 1
fi


# 2) xixa
edat=$1
if [ $edat -ge 18 ]
then
  echo "edat $edat és menor d'edat"

exit 0

