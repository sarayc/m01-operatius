#! /bin/bash
# @edt ASIX-M01
# Curs 2022-2023
# Març 2023
# 
# Descripció: exemples bucle while
# ---------------------------------------------------------

# 7) numera i mostra en majúscules l'entrada estàndard
num=1

while read -r line
do
  maj=$(echo $line | tr 'a-z' 'A-Z' < $line)
  echo  "$num: $line" | $maj
  ((num++))
done
exit 0

# --------------------------------------------------------
# 6) mostrar stdin linia a linia fins token FI
read -r line
while [ "$line" != "FI" ]
do
  echo "$line"
  read -r line
done
exit 0

# ---------------------------------------------------------
# 5) numerar stdin línia a línia 
num=1
while read -r line
do
  echo "$num: $line"
  ((num++))
done
exit 0

# -------------------------------------------------------
# 4) processar entrada estàndard linia a linia
while read -r line
do
  echo $line
done
exit 0

# --------------------------------------------------------
# 3) iterar per la llista d'arguments
while [ -n "$1" ]
do
  echo "$1 $# $*"
  shift
done
exit 0

# ---------------------------------------------------------
# 2) Comptador decreixent n(arg) ...0
MIN=0
num=$1
while [ $num -ge $MIN ]
do
  echo "$num"
  ((num--))
done
exit 0

# ---------------------------------------------------------

# 1) Programa que mostri els números del 1 al 10
MAX=10
num=1
while [ $num -le $MAX ]
do
  echo "$num"
  ((num++))
done
exit 0
