#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Saray Cordón Arnau
#
# 18-argswhile.sh [ -a -b -c -d ] -f file -n num arg...
# ----------------------------------------------------------------------
opcions=""
arguments=""
num=""
file=""

while [ -n "$1" ]
do
  case $1 in 
    "-a"|"-b"|"-c"|"-d")
      opcions="$opcions $1";;
    "-f")
      file=$2
      shift;;
    "-n")
      num=$2
      shift;;
    *)
      arguments="$arguments $1";;
  esac
  shift
done
echo "opcions: $opcions"
echo "file: $file"
echo "num: $num"
echo "arguments: $arguments"
exit 0
