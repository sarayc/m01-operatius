#! /bin/bash
# ASIX M01
# Curs 2022-2023
# 23/01/2023
#
# Validar que rep un argument i que és un directori i llistar-ne el contingut. 
# Ens digui si és un link, regular file, directori o altra cosa.

# sinopsis: prog dir
# ----------------------------------------------------------------------------
ERR_NARGS=1
ERR_NODIR=2
# 1) validar número d'arguments
if [ $# -ne 1 ]
then
  echo "Error: número arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# 2) validar que és un directori
directori=$1
if ! [ -d $directori ]
then
  echo "Error: no és un directori"
  echo "Usage: $0 dir"
  exit $ERR_NODIR
fi

# 3) xixa
llista=$(ls $directori)
for element in $llista
do
  if [ -L "$dir/$element" ] 
  then
    echo "$element és un Link"
  elif [ -f "$dir/$element" ]
  then
    echo "$element és un regular file"
  elif [ -d "$dir/$element" ]
  then
    echo "$element és un directori"
  else
    echo "$element és un altra cosa"
  fi 
done
exit 0
