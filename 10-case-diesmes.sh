#! /bin/bash
# ASIX M01
# Curs 2022-2023
# 23/01/2023
#
# Exemple case dies del mes
# ----------------------------------------------------------
ERR_ARGS=1
ERR_NOMES=2

# 1) Validar el número d'arguments
if [ $# -ne 1 ]
then
  echo "Error: número d'arguments no vàlid"
  echo "Usage: $0 mes"
  exit $ERR_ARGS
fi

# 2) validar número de mes
if ! [ $1 -ge 1 -a $1 -le 12 ]
then
  echo "Error: els mesos han de ser entre 1 i 12"
  echo "Usage: $0 mes"
  exit $ERR_NOMES
fi

# 2) xixa
mes=$1

case $mes in 
  "4"|"6"|"9"|"11")
    dies=30;;
  "2")
    dies=28;;
  *) 
    dies=31;;
esac
echo "El mes $mes té $dies dies"
exit  0
  
